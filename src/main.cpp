#include <iostream>
#include <vector>

#ifdef __unix__
#include <postgresql/libpq-fe.h>
#else
#include <libpq-fe.h>
#endif

const std::string kDbHostIP = "10.227.240.130";
const std::string kDbName = "pswa0409";
const std::string kDbUsername = "pswa0409";
const std::string kDbPassword = "ylSLRIPz";

int main(int argc, char** argv) {
  // Database
  std::string dbconn_str = "dbname=" + kDbName + " host=" + kDbHostIP +
      " user=" + kDbUsername + " password=" + kDbPassword +
      " connect_timeout=2";   // not recommended timeout less than 2s

  PGconn* dbconn = PQconnectdb(dbconn_str.c_str());
  if (PQstatus(dbconn) != CONNECTION_OK) {
    std::cerr << "Error when connecting to the database " + dbconn_str
              << std::endl;

    PQfinish(dbconn);  // even if fail, free allocated memory of PGconn object
    dbconn = nullptr;
    
    return -1;
  }

  // Set psw schema
  std::cout << "Setting schema to psw..." << std::endl;
  if (PQstatus(dbconn) == CONNECTION_OK) {
    PGresult* result = PQexec(dbconn, "SET search_path TO psw;");

    if (PQresultStatus(result) == PGRES_FATAL_ERROR) {
      std::cerr << "[QUERY] Error occurred when executing "
                   "'SET search_path TO psw;'\n" +
                   std::string(PQerrorMessage(dbconn))
                << std::endl;

      PQclear(result);

      PQfinish(dbconn);  // even if fail, free allocated memory of PGconn object
      dbconn = nullptr;
      
      return -1;
    } else {
      PQclear(result);
    }
  }

  // Confirm if the schema was changed
  if (PQstatus(dbconn) == CONNECTION_OK) {
    PGresult* result = PQexec(dbconn, "SHOW search_path;");

    if (PQresultStatus(result) == PGRES_FATAL_ERROR) {
      std::cerr << "[QUERY] Error occurred when executing "
                   "'SHOW search_path;'\n" +
                   std::string(PQerrorMessage(dbconn))
                << std::endl;

      PQclear(result);

      PQfinish(dbconn);  // even if fail, free allocated memory of PGconn object
      dbconn = nullptr;
      
      return -1;
    } else {
      std::string search_path = PQgetvalue(result, 0, 0);
      PQclear(result);

      std::cout << "Search path: " << search_path << std::endl;
    }
  }


  /***********************************
   * EXAMPLE QUERIES TO THE DATABASE *
   ***********************************/

  // - get all people in the database
  if (PQstatus(dbconn) == CONNECTION_OK) {
    PGresult* result = PQexec(dbconn, "SELECT * FROM people;");

    if (PQresultStatus(result) == PGRES_FATAL_ERROR) {
      std::cerr << "[QUERY] Error occurred when executing "
                   "'SELECT * FROM people;'\n" +
                   std::string(PQerrorMessage(dbconn))
                << std::endl;

      PQclear(result);

      PQfinish(dbconn);  // even if fail, free allocated memory of PGconn object
      dbconn = nullptr;

      return -1;
    } else {
      std::cout << std::endl << "All people saved in the database:"
                << std::endl;

      for (int i = 0; i < PQntuples(result); i++) {
        std::cout << PQgetvalue(result,i,0) << " "
                  << PQgetvalue(result,i,1) << " "
                  << PQgetvalue(result,i,2) << std::endl;
      }

      PQclear(result);
    }
  }

  PQfinish(dbconn);  // even if fail, free allocated memory of PGconn object
  dbconn = nullptr;

  return 0;
}

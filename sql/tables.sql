CREATE TABLE people (
  name varchar(100) NOT NULL,
  email varchar(50) NOT NULL UNIQUE,
  code varchar(11) PRIMARY KEY -- upXXXXXXXXX ~ 11 characters
);

CREATE TABLE staff (
  acronym varchar(5) NOT NULL UNIQUE,
  code varchar(11) REFERENCES people (code) PRIMARY KEY
);

CREATE TABLE student (
  code varchar(11) REFERENCES people (code) PRIMARY KEY
);

CREATE TABLE department (
  name varchar(100) NOT NULL,
  acronym varchar(5) PRIMARY KEY,
  head varchar(11) REFERENCES staff (code)
);

-- Add association relation between staff and department
-- (only possible to do after the department table was created)
ALTER TABLE staff ADD COLUMN
  member_department varchar(5) REFERENCES department (acronym);

CREATE TABLE course (
  name varchar(100) NOT NULL,
  acronym varchar(7) PRIMARY KEY,
  director varchar(11) REFERENCES staff (code)
);

CREATE TABLE enrolled (
  id SERIAL PRIMARY KEY,
  current_year integer NOT NULL,
  status varchar(20) NOT NULL,
  student_code varchar(11) REFERENCES student (code) NOT NULL,
  course_acronym varchar(7) REFERENCES course (acronym) NOT NULL
);
